import re


def main():
    # path = "/Users/aarash/Downloads/ficsgamesdb_201802_standard2000_nomovetimes_1539755.pgn"
    #
    # path3 = "/Users/aarash/Downloads/thule179-2016-internet-chess-games/2016_CvH.csv"
    #
    # path2 = "/Users/aarash/Downloads/MACH2/codebook.txt"
    # path_temp = "/Users/aarash/Downloads/temp.txt"


    # main_path = "/Users/aarash/Downloads/MainDataChess/ficsgamesdb_201801_chess_nomovetimes_1539757.pgn"
    # main_path = "/Users/aarash/Downloads/ficsgamesdb_2014_titlged_nomovetimes_1539817.pgn"

    date = "2016_Jan"

    # main_path = "/Users/aarash/Downloads/MainDataChess/ficsgamesdb_201801_chess_nomovetimes_1539757.pgn"
    # main_path = "/Users/aarash/Downloads/ficsgamesdb_2014_titlged_nomovetimes_1539817.pgn"
    main_path = "/Users/aarash/Courses/Courses/8/Data_Analysis/PROJECT_DATA/" + date + "/ficsgamesdb_" + date + ".pgn"

    # main_write_path = "/Users/aarash/Downloads/MainDataChess/clean_2018.pgn"
    main_write_path = "/Users/aarash/Courses/Courses/8/Data_Analysis/PROJECT_DATA/" + date + "/ficsgamesdb_" + date + "_edited.pgn"

    blackelo = re.compile("\[BlackElo \".*\"\]")
    whiterd = re.compile("\[WhiteRD \".*\"\]")
    blackrd = re.compile("\[BlackRD \".*\"\]")
    blackComp = re.compile("\[BlackIsComp \".*\"\]")
    whiteComp = re.compile("\[WhiteIsComp \".*\"\]")

    # res = blackrd.match("[BlackRD \"26.0\"]")
    #
    # print(res)
    # print("[BlackdRD \"26.0\"]")
    # print(re.match("\[BlackRD \".*\"\]", "[BlackdRD \"26.0\"]"))

    temp_write = open(main_write_path, 'w')

    with open(main_path, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            while blackrd.match(line) or whiterd.match(line):
                line = f.readline()
            if blackelo.match(line):
                temp_write.write(line)
                line = f.readline()
                while blackComp.match(line) or whiteComp.match(line):
                    line = f.readline()

            else:
                temp_write.write(line)
    temp_write.close()

main()