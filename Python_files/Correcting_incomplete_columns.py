import re


def main():
    # path = "/Users/aarash/Downloads/ficsgamesdb_201802_standard2000_nomovetimes_1539755.pgn"
    #
    # path3 = "/Users/aarash/Downloads/thule179-2016-internet-chess-games/2016_CvH.csv"
    #
    # path2 = "/Users/aarash/Downloads/MACH2/codebook.txt"
    # path_temp = "/Users/aarash/Downloads/temp.txt"

    date = "2018_Jan"

    # main_path = "/Users/aarash/Downloads/MainDataChess/ficsgamesdb_201801_chess_nomovetimes_1539757.pgn"
    # main_path = "/Users/aarash/Downloads/ficsgamesdb_2014_titlged_nomovetimes_1539817.pgn"
    main_path = "/Users/aarash/Courses/Courses/8/Data_Analysis/PROJECT_DATA/" + date + "/ficsgamesdb_" + date + ".pgn"

    # main_write_path = "/Users/aarash/Downloads/MainDataChess/clean_2018.pgn"
    main_write_path = "/Users/aarash/Courses/Courses/8/Data_Analysis/PROJECT_DATA/" + date + "/ficsgamesdb_" + date + "_cleaned.pgn"

    white = re.compile("\[White \".*\"\]")  # 0
    black = re.compile("\[Black \".*\"\]")  # 1
    whiteelo = re.compile("\[WhiteElo \".*\"\]")  # 2
    blackelo = re.compile("\[BlackElo \".*\"\]")  # 3
    whiteComp = re.compile("\[WhiteIsComp \".*\"\]")  # 4
    blackComp = re.compile("\[BlackIsComp \".*\"\]")  # 5
    timecontrol = re.compile("\[TimeControl \".*\"\]")  # 6
    date = re.compile("\[Date \".*\"\]")  # 7
    time = re.compile("\[Time \".*\"\]")  # 8
    whiteclock = re.compile("\[WhiteClock \".*\"\]")  # 9
    blackclock = re.compile("\[BlackClock \".*\"\]")  # 10
    eco = re.compile("\[ECO \".*\"\]")  # 11
    playcount = re.compile("\[PlyCount \".*\"\]")  # 12
    result = re.compile("\[Result \".*\"\]")  # 13
    moves = re.compile("1\. .*")  # 14

    # res = blackrd.match("[BlackRD \"26.0\"]")
    #
    # #print(res)
    # #print("[BlackdRD \"26.0\"]")
    # #print(re.match("\[BlackRD \".*\"\]", "[BlackdRD \"26.0\"]"))

    temp_write = open(main_write_path, 'w')

    state = 0


    with open(main_path, 'r') as f:
        while True:
            line = f.readline()
            if line == "\n":
                temp_write.write(line)
                line = f.readline()
            if not line:
                break
            if line.startswith("[Event") or \
                    line.startswith("[Site") or \
                    line.startswith("[FICSGamesDBGameNo") or \
                    line.startswith("[WhiteRD") or \
                    line.startswith("[BlackRD"):
                continue
            if white.match(line) and state == 0:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[White \"\"]\n")
                state = (state + 1) % 15
            if black.match(line) and state == 1:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[Black \"\"]\n")
                state = (state + 1) % 15
            if whiteelo.match(line) and state == 2:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[WhiteElo \"\"]\n")
                state = (state + 1) % 15

            if blackelo.match(line) and state == 3:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[BlackElo \"\"]\n")
                state = (state + 1) % 15

            if whiteComp.match(line) and state == 4:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[WhiteIsComp \"\"]\n")
                state = (state + 1) % 15

            if blackComp.match(line) and state == 5:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[BlackIsComp \"\"]\n")
                state = (state + 1) % 15

            if timecontrol.match(line) and state == 6:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[TimeControl \"\"]\n")
                state = (state + 1) % 15

            if date.match(line) and state == 7:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[Date \"\"]\n")
                state = (state + 1) % 15

            if time.match(line) and state == 8:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[Time \"\"]\n")
                state = (state + 1) % 15

            if whiteclock.match(line) and state == 9:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[WhiteClock \"\"]\n")
                state = (state + 1) % 15

            if blackclock.match(line) and state == 10:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[BlackClock \"\"]\n")
                state = (state + 1) % 15

            if eco.match(line) and state == 11:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[ECO \"\"]\n")
                state = (state + 1) % 15

            if playcount.match(line) and state == 12:
                temp_write.write(line)
                line = f.readline()
                #print(line)
                state = (state + 1) % 15
            else:
                temp_write.write("[PlyCount \"\"]\n")
                state = (state + 1) % 15

            if result.match(line) and state == 13:
                temp_write.write(line)
                line = f.readline()
                #print('shit')
                #print(line)
                if line == "\n":
                    temp_write.write(line)
                    line = f.readline()
                    #print('shit')
                    #print(line)

                state = (state + 1) % 15
            else:
                temp_write.write("[Result \"\"]\n")
                state = (state + 1) % 15

            if moves.match(line) and state == 14:
                temp_write.write(line)
                state = (state + 1) % 15
            else:
                temp_write.write("1. \n")
                state = (state + 1) % 15

    temp_write.close()

#main()


for i in range(59):
    print("hcharts[[", i, "]]", sep="")