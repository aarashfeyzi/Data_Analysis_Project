import re


def main():
    # path = "/Users/aarash/Downloads/ficsgamesdb_201802_standard2000_nomovetimes_1539755.pgn"
    #
    # path3 = "/Users/aarash/Downloads/thule179-2016-internet-chess-games/2016_CvH.csv"
    #
    # path2 = "/Users/aarash/Downloads/MACH2/codebook.txt"
    # path_temp = "/Users/aarash/Downloads/temp.txt"


    # main_path = "/Users/aarash/Downloads/MainDataChess/ficsgamesdb_201801_chess_nomovetimes_1539757.pgn"
    # main_path = "/Users/aarash/Downloads/ficsgamesdb_2014_titlged_nomovetimes_1539817.pgn"

    date = "2016_Jan"

    # main_path = "/Users/aarash/Downloads/MainDataChess/ficsgamesdb_201801_chess_nomovetimes_1539757.pgn"
    # main_path = "/Users/aarash/Downloads/ficsgamesdb_2014_titlged_nomovetimes_1539817.pgn"
    main_path = "/Users/aarash/Courses/Courses/8/Data_Analysis/PROJECT_DATA/" + date + "/ficsgamesdb_" + date + "_edited.pgn"

    # main_write_path = "/Users/aarash/Downloads/MainDataChess/clean_2018.pgn"
    main_write_path = "/Users/aarash/Courses/Courses/8/Data_Analysis/PROJECT_DATA/" + date + "/ficsgamesdb_" + date + "_2.pgn"

    blackrd = re.compile("\[BlackElo \".*\"\]")
    blackComp = re.compile("\[BlackIsComp \".*\"\]")
    whiteComp = re.compile("\[WhiteIsComp \".*\"\]")

    # res = blackrd.match("[BlackRD \"26.0\"]")
    #
    # print(res)
    # print("[BlackdRD \"26.0\"]")
    # print(re.match("\[BlackRD \".*\"\]", "[BlackdRD \"26.0\"]"))

    temp_write = open(main_write_path, 'w')

    with open(main_path, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            if line.startswith("[Event") or line.startswith("[Site") or line.startswith("[FICSGamesDBGameNo"):
                continue
            if blackrd.match(line):
                temp_write.write(line)
                line1 = f.readline()
                line2 = f.readline()
                if whiteComp.match(line1) and blackComp.match(line2):

                    temp_write.write(line1)
                    temp_write.write(line2)

                elif (not whiteComp.match(line1)) and blackComp.match(line1):

                    temp_write.write("[WhiteIsComp \"No\"]\n")
                    temp_write.write(line1)
                    temp_write.write(line2)

                elif (whiteComp.match(line1)) and not blackComp.match(line2):

                    temp_write.write(line1)
                    temp_write.write("[BlackIsComp \"No\"]\n")
                    temp_write.write(line2)

                elif (not whiteComp.match(line1)) and not blackComp.match(line2):

                    temp_write.write("[WhiteIsComp \"No\"]\n")
                    temp_write.write("[BlackIsComp \"No\"]\n")
                    temp_write.write(line1)
                    temp_write.write(line2)

            else:
                temp_write.write(line)

    temp_write.close()

main()